package ru.t1.karimov.tm.command.project;

import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT LIST BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = getProjectService().findOneByIndex(index);
        showProject(project);
    }

    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Show project by index.";
    }

}
