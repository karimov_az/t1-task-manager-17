package ru.t1.karimov.tm.command;

import ru.t1.karimov.tm.api.model.ICommand;
import ru.t1.karimov.tm.api.service.IServiceLocator;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute() throws AbstractException;

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        final String colonComma = (argument == null || argument.isEmpty()) ? " : " : " , ";
        if (name != null && !name.isEmpty()) result += name + colonComma;
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
