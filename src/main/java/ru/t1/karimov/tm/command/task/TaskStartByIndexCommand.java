package ru.t1.karimov.tm.command.task;

import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        getTaskService().changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @Override
    public String getDescription() {
        return "Start task by index.";
    }

}
