package ru.t1.karimov.tm.command.project;

import ru.t1.karimov.tm.exception.field.AbstractFieldException;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractFieldException {
        System.out.println("[PROJECT CLEAR]");
        getProjectService().clear();
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

}
