package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.command.AbstractCommand;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    AbstractCommand getCommandByArgument(String argument);

    AbstractCommand getCommandByName(String name) throws AbstractException;

    Collection<AbstractCommand> getTerminalCommands();

}
