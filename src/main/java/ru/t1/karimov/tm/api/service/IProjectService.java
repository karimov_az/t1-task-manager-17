package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.enumerated.Sort;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project create (String name, String description) throws AbstractFieldException;

    Project create (String name) throws AbstractFieldException;

    Project add(Project project) throws AbstractEntityNotFoundException;

    void clear();

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    void remove(Project project) throws AbstractEntityNotFoundException;

    boolean existsById(String id);

    Project findOneById(String id) throws AbstractFieldException;

    Project findOneByIndex(Integer index) throws AbstractFieldException;

    Project updateById(String id, String name, String description) throws AbstractException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractException;

    Project removeById(String id) throws AbstractFieldException;

    Project removeByIndex(Integer index) throws AbstractFieldException;

    Project changeProjectStatusById (String id, Status status) throws AbstractException;

    Project changeProjectStatusByIndex (Integer index, Status status) throws AbstractException;

    int getSize();

}
