package ru.t1.karimov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
